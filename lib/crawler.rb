require_relative 'page'
require_relative 'histogram'

'''
	Crawler - a class that crawls a site within a certain domain and
		gathers useful information for each page it can find by crawling
		from link to link
'''
class Crawler
	'''
		Initializes the pages hash and creates an instance of the
		Histogram class for a global histogram.
	'''
	def initialize
		@pages = Hash.new

		# Passing true signifies that this is a global histogram
		# and shouldn't be counted in the total document count.
		@global_histogram = Histogram.new (true)
	end

	'''
		Crawls a page and then recursively crawls its children
		if relevant to the constraints of the crawler.

		@param uri The uri whose page to retrieve
		@param domain The domain constraint to prevent unbounded crawling
	'''
	def crawl (uri, domain)
		if page_added? uri
			# Increment the number of references to the current root page
			add_page_reference(uri)
		else
			@uri = uri

			puts "crawling page #{uri}"

			# Create the root page (from this scope) and handle each
			# word in a block to add them to a global histogram
			root = Page.new(uri, domain) do |word|
				@global_histogram.add word
			end

			# Cache the page for later and so that duplicates can be avoided
			add_page(root)

			# Recursively crawl all child links in the page
			root.children.each do |child_uri|
				crawl(child_uri, domain)
			end
		end
	end

	'''
		Generates a report with local histograms and a global histogram.
	'''
	def report
		# For each page, generate a new file in the root directory
		# and output the URI, number of references, and the histogram
		@pages.each do |uri, page|
			# Example: science-technology_about.txt
			filename = "#{URI.parse(uri).path.gsub("/", "_")}.txt"[1..-1]

			output = File.open(filename, "w")

			output.puts "URL:", uri
			output.puts "Number of references:", page.references
			output.puts
			output.puts "Histogram:"

			# each_word is user-defined and enumerates each word based
			# on its TF-IDF score, in descending order.
			page.histogram.each_word { |word, frequency| 
				output.print "#{word}: #{frequency}"
				output.puts
			}

			output.close
		end

		# Generate a similar report for the global histogram.
		output = File.open("global.txt", "w")
		output.puts "Global histogram:"

		@global_histogram.each_word { |word, frequency|
			output.puts "#{word}: #{frequency}"
		}
		output.close
	end

	# Cache the page for later
	def add_page (page)
		@pages[page.uri] = page
	end

	# Returns true of the page is already being tracked.
	def page_added? (uri)
		@pages[uri]
	end

	# Increment the reference count for a URI
	def add_page_reference(uri)
		@pages[uri].add_reference
	end

	private :add_page, :page_added?, :add_page_reference
end

if __FILE__ == $0
	crawler = Crawler.new

	uri = "http://www.usm.edu/science-technology"
	domain = "usm.edu/science-technology"

	crawler.crawl(uri, domain)
	crawler.report
	
	puts "Done! You can find the output files in the root directory."
end