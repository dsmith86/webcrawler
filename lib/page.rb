require 'nokogiri'
require 'open-uri'
require 'uri'

require_relative 'histogram'

'''
	Page - a class that stores useful information for a web page.
'''
class Page
	attr_reader :uri, :children, :references, :histogram

	def initialize (uri, domain)
		@uri = uri
		@domain = domain

		# Passing false ensures that this is treated as a local histogram.
		@histogram = Histogram.new (false)

		# Getting to this page means that there is a reference to it.
		@references = 1

		document = Nokogiri::HTML(open(uri))

		# Retrieve the host prt of the HTTP URI.
		host = URI.parse(uri).host

		# Get all links
		@children = document.css("a").map { |link|
			child_uri = String.new

			# The link contains an href component.
			if (href = link.attr("href"))
				# The link is absolute.
				if href =~ URI::DEFAULT_PARSER.regexp[:ABS_URI]
					child_uri = href
				# The link is relative (implementation specific in this case).
				elsif href =~ /^\/.+$/
					child_uri = "http://#{host}#{href}"
				end

			end

			# String is not empty
			if !child_uri.empty?
				# URI satisfied the required domain constraint.
				if child_uri.include? domain
					child_uri
				end
			end
		}.compact # remove nil elements.

		# Get words in a paragraph or any of the six header types.
		p_words = document.search('div.main-wrapper p, div.main-wrapper h1, div.main-wrapper h2, div.main-wrapper h3, div.main-wrapper h4, div.main-wrapper h5, div.main-wrapper h6').map do |blob|
			blob.text
		end

		# For each string of words,
		p_words.each do |blob|
			# Get each alphanumeric word symbol.
			blob.scan(/[\w']+/).each do |word|
				# Only process words with only letters.
				if word =~ /^[A-Za-z]+$/
					# Add the word to the local histogram.
					@histogram.add word

					# Yield the word back to the caller.
					yield word
				end
			end
		end
	end

	# Increment the number of references to this page.
	def add_reference
		@references += 1
	end
end