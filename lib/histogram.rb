'''
	Histogram - a class that tracks relative word frequencies per document,
		with some additional information about the set of all known documents.
		This can be used to generate the TF-IDF of a word with respect to the
		set of known documents.
'''
class Histogram
	# The total number of documents containing some key
	@@global_frequency = Hash.new(0)

	# The total number of documents tracked at runtime
	@@histogram_count = 0

	def initialize(global)
		# A hash where words are the keys and their count in the current
		# document are the values
		@words = Hash.new(0)

		# The total number of words in the document
		@word_count = 0

		# Whenever a new histogram is created, the total number of known
		# histograms is incremented.
		@@histogram_count += 1 unless global
	end

	'''
		Yields each word in a block along with their TF-IDF.
			They are yielded in descending order of TF-IDF values. 
	'''
	def each_word
		# First, the words are ranked according to their TF-IDF
		ranked_words = @words.sort_by { |word, frequency|
			tf_idf(word)
		}

		# Yields each in descending order.
		ranked_words.reverse_each { |word, frequency| 
			yield word, tf_idf(word)
		}
	end

	# Adds the word to the histogram and updates frequencies as appropriate
	def add (word)
		# Convert the word to downcase unless it's all upcase.
		# Converting to downcase increases the accuracy of the histogram, while retaining
		# the stylistic representation of acronyms and initialisms.
		word.downcase! unless word == word.upcase

		# Add to the global document frequency if the word is newly tracked
		# in this histogram.
		@@global_frequency[word] += 1 unless @words[word] > 0

		# Increment the raw word frequency for this histogram.
		@words[word] += 1

		# Increment the total word count for this histogram.
		@word_count += 1
	end

	# Returns the relative frequency of the word in the current document.
	def tf (word)
		@words[word].to_f / @word_count
	end

	# Returns a frequency that relates to the ratio of the number of documents
	# present to the number of documents containing the specified word.
	def idf (word)
		Math.log(@@histogram_count / (1 + @@global_frequency[word]).to_f)
	end

	# Returns the TF-IDF of a word, which specifies how relatively important the word is.
	def tf_idf (word)
		tf(word) * idf(word)
	end

	private :tf, :idf, :tf_idf
end