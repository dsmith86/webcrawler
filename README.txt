###############################################################################
AUTHOR: 	Daniel Smith
DATE:		Thursday, April 30, 2015
PURPOSE:	This program is a web crawler that is made to crawl
			recursively through all pages publicly linked to by some page.
			This crawler is tailored specifically to the content organization
			of usm.edu/science-technology/ and its subpages, but it may work
			elsewhere.
###############################################################################

============
Introduction
============

This web crawler uses the Ruby gem Nokogiri to easily glean the relevant information
from a web page in order to crawl using anchor tags, commonly called hypertext links.

Running this program will first crawl the URL specified in the main part of the program
and then find all pages on the specified domain.

The program will then generate a report for each page as well as some global statistics for the site.

=====
Setup
=====

This project requires you to have Rubygems installed, as well as the Nokogiri gem.

Alternatively, you can have Rubygems installed as well as the Bundler gem, which can then be used to find and apply necessary dependencies.

You can bundle up the necessary dependencies with the following Bundler command in the terminal:

	////////////////
	> bundle install
	\\\\\\\\\\\\\\\\

This will create the Gemfile.lock file, and the project will be ready to use.

Otherwise, just install the nokogiri gem manually:

	//////////////////////
	> gem install nokogiri
	\\\\\\\\\\\\\\\\\\\\\\

If this fails and you've tried all other possible avenues, you can try the following:

	///////////////////////////////////
	> gem install nokogiri --prerelease
	\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

This is what ended up working for me.

Then, you can simply run the program by typing:

	/////////////////////
	> ruby lib/crawler.rb
	\\\\\\\\\\\\\\\\\\\\\

from the root directory of the project.

After running, the 

========
Analysis
========

Based on the reports that are generated, it is very apparent that the TF-IDF metric is great for ranking words based on their relative "importance". The TF-IDF metric is great when the histogram is specific to a document, but it should be noted that it is useless when you have a global histogram that is a superset of all of the other histograms. This is because the TF-IDF finds the importance of word based on how infrequent it is among a set of documents.

The output files clearly shows this, as the pages clearly can be represented by the top words shown in the output.

Each line in the histograms consists of the word and its relative TF-IDF value.